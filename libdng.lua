--
-- RTT next generation deployement
--
-- very much work in progress.

local rttlib = require "rttlib"
local rttros = require "rttros"
local utils = require "utils"
local json = require "json"
local umf = require "umf"
local ac = require "ansicolors"
local map, foreach, filter = utils.map, utils.foreach, utils.filter

--local strict = require "strict"

module("libdng", package.seeall)

--- Some logging helpers
max_width=120

local d=false      -- no deployer available initially

function err(str)
   str = "\nDNG err: " .. str
   if rttlib.color then str = ac.red(ac.bright(str)) end
   error(str)
end

function log(...)
   local outstr = table.concat({...}, ' ')
   io.stdout:write(
      utils.rpad(outstr,
		 max_width-10, ' ', #utils.strip_ansi(outstr)))
end

function logOK()
   if rttlib.color then print(ac.green(ac.bright("[OK]")))
   else print("[OK]") end
end

function logFail()
   if rttlib.color then print(ac.red(ac.bright("[FAILED]")))
   else print("[FAILED]") end
end

--- build a table of peers
function get_peers()
   return rttlib.mappeers(function (tc) return tc end, d)
end

--- Is this a deployer?
function is_deployer(depl)
   if rttlib.rtt_type(depl) ~= 'TaskContext' then return false end
   if not depl:hasOperation("loadComponent") then return false end
   return true
end

--- Duck-check if we have a deployer around.
-- We really need one!
function check_deployer()
   if not d then
      d = rttlib.findpeer("deployer", rtt.getTC()) or rttlib.findpeer("Deployer", rtt.getTC())
   end
   if not is_deployer(d) then
      err("no valid deployer set, use dng.set_deployer(deployer)")
   end
end


--------------------------
-- uMF validation model --
--------------------------

AnySpec=umf.AnySpec
NumberSpec=umf.NumberSpec
StringSpec=umf.StringSpec
BoolSpec=umf.BoolSpec
EnumSpec=umf.EnumSpec
TableSpec=umf.TableSpec
ClassSpec=umf.ClassSpec
ObjectSpec=umf.ObjectSpec

uoo_type = umf.uoo_type
instance_of = umf.instance_of

config = umf.class("config")
import = umf.class("import")
create = umf.class("create")
connect = umf.class("connect")
prop_set = umf.class("prop_set")
call = umf.class("call")
port_write = umf.class("port_write")


function is_config(x) return uoo_type(x) == 'instance' and instance_of(config, x) end
function is_import(x) return uoo_type(x) == 'instance' and instance_of(import, x) end
function is_create(x) return uoo_type(x) == 'instance' and instance_of(create, x) end
function is_connect(x) return uoo_type(x) == 'instance' and instance_of(connect, x) end
function is_prop_set(x) return uoo_type(x) == 'instance' and instance_of(prop_set, x) end
function is_call(x) return uoo_type(x) == 'instance' and instance_of(call, x) end
function is_port_write(x) return uoo_type(x) == 'instance' and instance_of(port_write, x) end

-- import spec
import_spec = ObjectSpec {
   name='import',
   type = import,
   sealed='both',
   array={ StringSpec{} },
}

-- switch conf
switch_conf_spec=TableSpec{
   name='switch_conf',
   sealed='both',
   array={StringSpec{}}
}

-- 'create' spec
create_spec = ObjectSpec {
   name='create',
   type=create,
   sealed='both',
   dict={
      name=StringSpec{},
      type=StringSpec{},
      period=NumberSpec{},
      priority=NumberSpec{min=0, max=99},
      peers=TableSpec{ name='peer list', sealed='both', array={ StringSpec{} } },
      schedt=NumberSpec{},
   },
   optional={'period', 'priority', 'peers', 'schedt' },
}


-- 'connect' and 'connpol' spec
connpol_spec = umf.TableSpec{
   name='connection policy',
   sealed='array',
}

-- 'connect' spec
connect_spec = ObjectSpec{
   name='connect',
   type=connect,
   sealed='both',
   dict={
      from=StringSpec{},
      to=StringSpec{},
      connpol=connpol_spec,
   },
   optional={ 'connpol' },
}

-- disconnect_spec tbd.

-- 'port_write'
port_write_spec = ObjectSpec {
   name='port_write',
   type=port_write,
   sealed='dict',
   dict={port=StringSpec{}}
}

-- 'prop_set'
prop_set_spec = ObjectSpec {
   name='prop_set',
   type=prop_set,
   sealed='both',
   dict={
      component = StringSpec{},
      value = AnySpec{},
   },
}

call_spec = ObjectSpec {
   name='call',
   type=call,
   sealed='dict',
   dict={ op=StringSpec{} },
}

-- print("prop_set_spec", prop_set_spec)

-- configuration spec
config_spec = ObjectSpec {
   name='config',
   type=config,
   sealed='both',

   dict={
      pre_conf_state=switch_conf_spec,
      post_conf_state=switch_conf_spec,
   },
   optional={"pre_conf_state", "post_conf_state"},

   -- config actions:
   array = { import_spec, create_spec, connect_spec, prop_set_spec,  port_write_spec, call_spec },
}

-- allow recursive nesting of configs not now, maybe later. What would
-- be the meaning? Apply all starting from top down?
config_spec.dict.__other={ config_spec }


--------------------------
-- deployment functions --
--------------------------

--- Write properties of component to file using json.
-- @param comp TaskContext
-- @param filename file name to write names.
function write_props_json(comp, filename)
   local res = {}
   local f = io.open(filename, "w")
   if not f then error("failed to open file " .. filename .. " for writing") end
   local props = comp:getProperties()
   for _, p in ipairs(props) do
      res[p:info().name] = rttlib.var2tab(p:get())
   end
   f:write(json.encode(res))
   f:close()
end

function dump_props(peers, dir, type)
   if not utils.file_exists(dir) then
      err("dump_props: non-existing target directory "..dir)
   end
   utils.foreach(function(pname, comp)
		    write_props_json(comp, dir.."/"..pname..".json")
		 end, peers)
end

--- Read properties from file and apply them to component.
--
function load_props_json(comp, filename)
   local f = io.open(filename, "r")
   if not f then error("failed to open file " .. filename .. " for reading") end
   local ptab = json.decode(f:read("*a"))
   local props = comp:getProperties()

   for _, p in ipairs(props) do
      local name = p:info().name
      local newval = ptab[name]
      if newval~=nil then
	 if rtt.Variable.isbasic(p:getRaw()) then p:set(newval)
	 else p:getRaw():fromtab(newval) end
      else
	 log("Warning: no configuration for property " .. comp:getName() .. p:info().name)
      end
   end
   return true
end

--- move to rttlib
function has_service(comp, srvname)
   return utils.table_has(comp:provides():getProviderNames(), srvname)
end

--- Read properties from cpf files.
function load_props_cpf(comp, filename)
   local name=comp:getName()
   if not has_service(comp, "marshalling") then
      if not d:loadService(name, "marshalling") then
	 err("failed to load marshalling service into "..name)
	 return false
      end
   end
   if not comp:provides("marshalling"):loadProperties(filename) then
      err("loadProperties failed to configure "..name.." from file "..filename)
   end
   return true
end


--- Recursively expand all parameters in string types.
-- check for cycles (could be caused by umf classes)
-- @param ass assembly
-- @param params table of parameters
function expand_params(ass, params)
   local seen={}

   local function __expand_params(ass,params)
      for k,v in pairs(ass) do
	 if type(v)=='string' then ass[k]=utils.expand(v, params)
	 elseif type(v)=='table' and not seen[v] then
	    seen[v] = true
	    __expand_params(v, params)
	 end
      end
   end
   __expand_params(ass,params)
end

--- Check wether a component type is known.
-- @return true or false
local function component_known(type)
   local comps = d:getComponentTypes()
   return utils.table_has(comps:var2tab(), type)
end

--- Validate and expand file spec.
-- This function expands the package# syntax and checks wether
-- files exists.
-- @return filename
-- @return file extension
function check_filespec(str)
   if type(str) ~= 'string' then return false end
   local _,_,filename=string.find(str, "file://(.*)")
   if not filename then err("invalid filespec " .. str) end

   -- package local syntax
   local ret,_,pack,file = string.find(filename,"(.+)#(.+)")
   if ret~=nil then
      ret, packpath = pcall(rttros.find_rospack, pack)
      if not ret then
	 err("rospack_find failed to locate package "..pack)
      end
      filename = packpath.."/"..file
   end

   if not utils.file_exists(filename) then
      err("non-existing configuration file "..filename.. "("..str..")")
   end

   -- determine file extension
   local _,_,fileext=string.find(filename, "%.(%a+)$")
   return filename, fileext
end

--- Safely get a property from a component.
-- @param comp tc
-- @param name name of desired property
-- @return property or false
local function get_prop(comp, name)
   local ret, val = pcall(rtt.TaskContext.getProperty, comp, name)
   if not ret then return false end
   return val
end

--- Switch state of a component.
-- ignore ourself in _default
-- @param comp TaskContext
-- @param des_state desired new state
function switch_state(comp, des_state)
   if comp:getState() == des_state then return true end
   if des_state == 'Ignore' then return true end

   log("switching "..comp:getName().." to state "..des_state)

   if des_state == 'Running' then
      if comp:getState() == 'PreOperational' then
	 if not comp:configure() then return false end
      end
      if comp:getState() == 'Stopped' then
	 if not comp:start() then return false end
      end
   elseif des_state == 'Stopped' then
      if comp:getState() == 'PreOperational' then
	 if not comp:configure() then return false end
      elseif comp:getState() == 'Running' then
	 if not comp:stop() then return false end
      end
   elseif des_state == 'PreOperational' then
      if comp:getState() == 'Running' then
	 if not comp:stop() then return false end
      end
      if comp:getState() == 'Stopped' then
	 if not comp:cleanup() then return false end
      end
   else
      logFail()
      err("switch_state: unknown desired state '"..tostring(des_state).."'")
      return false
   end
   logOK()
   return true
end

--- { default="S", "compA:R", "compB:U", "compC:S" }
-- or Running, PreOperational, Stopped,
--
local key_name_tab = {
   R='Running', Running='Running', S='Stopped', Stopped="Stopped",
   U='PreOperational', PreOperational='PreOperational',
   Unconfigured='PreOperational', I='Ignore', Ignore='Ignore'
}

-- ignore deployer, lua repl and ourself
local switch_ignore = { deployer=true, lua=true, [rtt.getTC():getName()]=true, coordinator=true }

function switch_conf(spec)
   local function key_to_state(key) return key_name_tab[key] end
   local peers = get_peers()
   local processed={} -- collect the peers mentioned in spec here.
   local def_state = "Ignore" -- default

   for _,entry in ipairs(spec) do
      local _,_,compname, state_key = string.find(entry, "^([%a%d_%./]+):(%a+)")

      -- validate
      if not compname then err("switch_conf: invalid spec entry "..entry) end
      local des_state = key_to_state(state_key)
      if not des_state then err("switch_conf: invalid target state key "..state_key.." for "..compname) end

      if compname == '_default' then
	 def_state = des_state
      else
	 if not peers[compname] then
	    err("switch_conf: unknown peer "..compname.." in spec entry "..entry)
	 end

	 -- Entry Ok, execute...
	 if not switch_state(peers[compname], des_state) then
	    err("switch_conf: failed to component "..compname.. " to " ..des_state)
	 end
	 processed[compname]=true
      end
   end

   -- apply default action to all unmentioned
   for n,p_tc in pairs(peers) do
      if not (processed[n] or switch_ignore[n]) then
	 if not switch_state(p_tc, def_state) then
	    err("switch_conf: failed to switch component "..
		n.. " to default " ..def_state)
	 end
      end
   end
end

--- Configure the properties of a component
function configure_props(c, conf)

   local cname = c:getName()
   print("configuring "..cname, conf)

   local filename, fileext = check_filespec(conf)

   if filename then -- configuration from a file?
      log("Configuring component "..cname.. " from file "..filename)
      if fileext == "json" then load_props_json(c, filename)
      elseif fileext == 'cpf' then load_props_cpf(c, filename)
      else
	 err("Unknown conf file extension "..fileext.." to be applied to "..
	     cname.. " (file: "..filename..")")
	 logFail()
	 return false
      end
      logOK()
   else -- configure from table
      for pname, val in pairs(conf) do
	 log("Configuring property "..cname.. "." .. pname.." with "..utils.tab2str(val))
	 local p = get_prop(c, pname)
	 if not p then
	    logFail()
	    err("configure: component " .. cname .. " has not property "..pname)
	    return false
	 end

	 if type(val)=='table' then p:fromtab(val) else p:set(val) end
	 logOK()
      end
   end
   return true
end

--- Import a list of packages packages.
-- @param imports list of package names to import
-- @return true if successfull, false otherwise
local function import_package(imports)
   local res=true
   for i,pack in ipairs(imports) do
      log("Importing package " .. ac.magenta(pack))
      if d:import(pack) then logOK()
      else
	 logFail()
	 res=false
      end
   end
   return res
end


--- Create a component and add it to the peers table.
-- @param compspec component specification.
local function component_create(compspec)
   local res=true
   compspec.period = compspec.period or 0
   compspec.prio = compspec.prio or 0
   compspec.schedt = compspec.schedt or rtt.globals.ORO_SCHED_OTHER
   compspec.peers = compspec.peers or {}
   compspec.services = compspec.services or {}

   log("Creating component " ..
       ac.bright(ac.green(compspec.name))
    .. " (" .. ac.magenta(compspec.type) .. ")")
   if not d:loadComponent(compspec.name, compspec.type) then
      logFail()
      res=false
   else
      logOK()
      peers[compspec.name] = d:getPeer(compspec.name)
   end

   d:setActivity(compspec.name, compspec.period, compspec.prio, compspec.schedt)
   for _,p in ipairs(compspec.peers) do
      if not d:addPeer(compspec.name, p) then err("failed to add peer "..p.." as a peer of "..compspec.name) end
   end
   local avail_srv=rtt.services()
   for _,srv in ipairs(compspec.services) do
      if not utils.table_has(avail_srv, srv) then
	 err("failed to load service "..srv.." into component "
	     ..compspec.name.." - unknown service.")
      else
	 if not d:loadService(compspec.name, srv) then
	    err("failed to load service "..srv.." into component "
		..compspec.name.." - loading failed.")
	 end
      end
   end
   return res
end



local cp=rtt.Variable("ConnPolicy")
local def_cp = cp:var2tab()

--- Create connections.
-- @param conns table of connection specs
local function create_conns(conns)
   for i,c in ipairs(conns) do
      cp:fromtab(def_cp)
      if c.connpol then cp:fromtab(c.connpol) end
      -- ros?
      if cp.transport==3 then
	 log("Connecting " .. c.from .. " -> " .. c.to.." (ROS)")
	 if cp.name_id == "" then cp.name_id=c.to end
	 if not d:stream(c.from, cp) then logFail() else logOK() end
      else
	 log("Connecting " .. c.from .. " -> " .. c.to)
	 if not d:connect(c.from, c.to, cp) then logFail() else logOK() end
      end
   end
   return true
end

--- Create components.
-- @param comps create spec table of assembly
local function components_create(comps)
   foreach(
      function(compspec)
	 if not component_known(compspec.type) then
	    err("component type " .. compspec.type .. " unknown. missing import?")
	 end
	 if not component_create(compspec) then
	    err("creating failed. bailing out.")
	 end
      end, comps)
   return true
end

--- memoize?
local function find_op(opspec)
   local idx = utils.split(opspec.op, "%.")
   local peers = get_peers()
   local ret = peers[table.remove(idx, 1)] -- this is the component at first pos!

   while #idx > 1 do
      ret = ret:provides(table.remove(idx, 1))
   end
   return ret:getOperation(idx[1])
end

--- Call an operation. TODO: return value specification.
local function call_op(opspec)
   log("calling operation "..opspec.op)
   local suc, op = pcall(find_op, opspec)
   if not suc then
      logFail(); log(msg); return false;
   end
   op(unpack(opspec))
   logOK()
   return true
end


--- Port_call stuff

--- Generate a fast port writer.
-- Generate a function that takes a Lua value (table or basic) and
-- writes that to the given port.
function gen_port_writer(port)
   sample=rtt.Variable(port:info().type)
   local assign_val
   if sample:isbasic() then
      assign_val = function(val)
		      return pcall(rtt.Variable.assign, sample, val)
		   end
   else
      assign_val = function(val)
		      return pcall(rttlib.varfromtab, sample, val)
		   end
   end
   -- this is the writer function
   return function (val)
	     local suc, ret = assign_val(val)
	     if not suc then err("put_val_on_port failed: ".. ret) end
	     port:write(sample)
	  end
end

--- Localize a port by name "comp.[service.]*portname"
-- @param pspec location specifier
-- @return port or fail
local function find_port(pspec)
   local idx = utils.split(pspec, "%.")
   local peers = get_peers()
   local ret = peers[table.remove(idx, 1)] -- this is the component at first pos!

   while #idx > 1 do ret = ret:provides(table.remove(idx, 1)) end
   return ret:getPort(idx[1])
end

local conn_ports={} -- cache
function get_conn_port_writer(pspec)
   local pw = conn_ports[pspec]
   if pw then return pw end

   -- slowpath
   log("creating port-connection to "..pspec)
   local suc, ptgt = pcall(find_port, pspec)
   if not suc then
      logFail(); log(ptgt); return false;
   end
   p = rttlib.port_clone_conn(ptgt)
   pw = gen_port_writer(p)
   conn_ports[pspec] = pw
   logOK();
   return pw
end


--- Apply a configuration.
function apply_conf(conf, peers)
   check_deployer() -- sanity check. we have a deployer

   if conf.pre_conf_state then switch_conf(conf.pre_conf_state) end

   -- process import's
   foreach(function (imp)
	      if not import_package(imp) then err("package import failed") end
	   end, filter(is_import, conf))

   -- process create's
   components_create(filter(is_create, conf))

   -- process connects's
   if not create_conns(filter(is_connect, conf)) then
      err("creating connections failed")
   end

   -- do port_write's
   local peers = get_peers()

   foreach(function(pw)
	      writer = get_conn_port_writer(pw.port)
	      if not writer then err("failed to create a port writer "..pw.port) end
	      writer(pw[1])
	   end, filter(is_port_write, conf))

   -- process prop_sets's
   foreach(function (ps)
	      local c = peers[ps.component]
	      if c==nil then err("prop_set: unkown component "..ps.component) end
	      if not configure_props(peers[ps.component], ps.value) then
		 err("prop_set: failed for component"..ps.component)
	      end
	   end, filter(is_prop_set, conf))

   -- process prop_sets's
   foreach(function (cl)
	      if not call_op(cl) then
		 err("calling op failed")
	      end
	   end, filter(is_call, conf))

   if conf.post_conf_state then switch_conf(conf.post_conf_state) end
   return true
end


--- Create a Configurator component
-- @param conf_file file name with configuration
-- @param name desired name of configuration component
-- @returns configurator
function launch_configurator(conf_file, name)
   check_deployer()
   name = name or "configurator"
   local conf_comp= rttros.find_rospack("dng").."/configurator.lua"

   if not d:loadComponent(name, "OCL::LuaComponent") then
      err("Failed to create Configurator Lua component")
   end
   cfgtr = d:getPeer("configurator")
   d:addPeer("configurator", "Deployer")
   cfgtr:exec_file(conf_comp)
   cfgtr:getProperty("conf_file"):set(conf_file)
   cfgtr:configure()
   cfgtr:start()
   return cfgtr
end

--- Validate a system configuration
-- @param assembly
function check(conf_file, verbose)
   local suc, model = pcall(dofile, conf_file)
   if not suc then
      print(model)
      print("dng: failed to load config file")
      return false
   end
   if not umf.instance_of(config, model) then
      print("invalid model - not a 'conf'")
      return false
   end

   local _, vres = umf.check(model, config_spec, verbose)
   if vres.err>0 then return false end
   return true
end

--- Launch a conf_file-fsm combo meal
function launch(conf_file, sim)
   if not check(conf_file, true) then return false end
   return launch_configurator(conf_file)
end

-- local function check_params(ass, param)
--    for i,p in ipairs(ass.parameters) do
-- 	 -- Ok:
-- 	 -- 1. optional and param[p.name]==nil
-- 	 -- 2. type(param[p.name]) == p.type

-- 	 if p.optional and not p.default then
-- 	    err("optional parameter "..p.name.." (type " ..p.type..") without default value")
-- 	    return false
-- 	 end

-- 	 if not p.optional and param[p.name]==nil then
-- 	    err("missing non-optional parameter "..p.name.." (should be of type " ..p.type..")")
-- 	    return false
-- 	 end

-- 	 if p.optional and param[p.name]==nil then
-- 	    param[p.name]=p.default
-- 	 end

-- 	 if type(param[p.name]) ~= p.type then
-- 	    err("invalid type of parameter "..p.name.."="..tostring(param[p.name])
-- 	     .." (should be of type " ..p.type..")")
-- 	    return false
-- 	 end
--    end
--    return true
-- end

--------------------
-- Misc functions --
--------------------




--- Autoconnect stuff. Use only to port legacy deployments.
--- Autoconnect two components which each other.
-- @param compA first component
-- @param compB second component
-- @return true or false
function autoconnect_two(compA, compB)
   function autoconnect_compatible(pA, pB)
      local pAinf, pBinf = pA:info(), pB:info()
      if pAinf.name == pBinf.name and
	 pAinf.type == pBinf.type and
	 pAinf.porttype ~= pBinf.porttype then
	 return true
      else return false end
   end

   if compA == compB then
      print(ac.red("Error: refusing to autoconnect "..compA:getName().." to itself"))
      return
   end

   local pA_names = compA:getPortNames()
   local pB_names = compB:getPortNames()

   utils.foreach(
      function (pname)
	 if not utils.table_has(pB_names, pname) then return end
	 local pA = compA:getPort(pname)
	 local pB = compB:getPort(pname)
	 if not autoconnect_compatible(pA, pB) then
	    return
	 end
	 if not pA:connect(pB) then
	    print(ac.red("Failed to autoconnect "..compA:getName().."."..pname.."->"..compB:getName()..".".. pname))
	 else
	    print("Autconnected "..compA:getName().."."..pname.." -> "..compB:getName().."."..pname)
	 end
      end, pA_names)
end

--- Connect all ports of a component to a list of other components.
-- @param compA component to connect from/to.
-- @param comp_list table of components to connect to from/to compA.
function autoconnect(compA, comp_list)
   utils.foreach(
      function (compB)
	 autoconnect_two(compA, compB)
      end, comp_list)
end
