
require "rttlib"
require "utils"


iface_spec = {
   ports={
      { name='inp', datatype='int', type='in+event', desc="incoming event port" },
      { name='msg', datatype='string', type='in+event', desc="incoming messages" },
      { name='outp', datatype='int', type='out', desc="outgoing data port" },
   },
   
   properties={
      { name='inc', datatype='int', desc="this value is added to the incoming data each step" }
   }
}

--- component global
iface=rttlib.create_if(iface_spec)
iface.props.inc:set(1)
inc = false

function configureHook()
   iface=rttlib.create_if(iface_spec)
   inc = iface.props.inc:get()
   return true
end

function startHook()
   iface.ports.outp:write(1)
   return true
end

function updateHook()
   local fs, val
   fs, val = iface.ports.inp:read()
   if fs=='NewData' then iface.ports.outp:write(val+inc) end

   fs, val = iface.ports.msg:read()
   if fs=='NewData' then print("received msg", val) end
end

function cleanupHook()
   rttlib.tc_cleanup()
end
