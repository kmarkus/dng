
return config {
   create_comps = config {
      import { "ocl" },
      create { name="l1", type="OCL::LuaComponent" },
      create { name="l2", type="OCL::LuaComponent" },
   },

   load_behavior = config {
      call { op="l1.exec_file", "tests/simple_comp.lua" },
      call { op="l2.exec_file", "tests/simple_comp.lua" },
      post_conf_state = { "l1:S", "l2:S" },
   },

   configure = config {
      prop_set { component="l1", value="file://dng#tests/cpf/l1.json" },
      prop_set { component="l2", value={ inc=3 } },
   },
   
   connect = config {
      connect { from="l1.outp", to="l2.inp" },
      connect { from="l2.outp", to="l1.inp" },
   },

   run = config{
      post_conf_state = { "_default:R" },
   },

   stop = config{
      pre_conf_state = { "_default:S" },
   },

   cleanup = config {
      pre_conf_state = { "_default:S" },
      call { op="deployer.unloadComponents" }
   },

   sayhi = config {
      port_write {port="l1.msg", "hi from configurator" }
   },

   sayho = config {
      port_write {port="l2.msg", "ho from configurator"}
   }
}