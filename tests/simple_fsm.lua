require("rfsmpp")
require("rfsm_checkevents")
require("rfsm_proto")
rfsm_proto.install{allow_send=true}

local state, trans, conn=rfsm.state, rfsm.trans, rfsm.conn

--- a conveniance function that generates a state in whose entry
-- function the given event is raised using 'emit_conf'
function gen_emit_state(event)
   assert(type(emit_event)=='function', "configuration function 'emit_event' not a function (or undefined)")
   return state {
      entry=function() emit_event(event) end
   }
end

return state {
   dbg = rfsmpp.gen_dbgcolor("Coordinator", { STATE_ENTER=true, STATE_EXIT=false}, false),

   -- deployment subfsm
   deploy = state {
      create_comps = gen_emit_state("create_comps"),
      load_behavior = gen_emit_state("load_behavior"),
      configure = gen_emit_state("configure"),
      connect = gen_emit_state("connect"),
      run = gen_emit_state("run"),

      trans{ src='initial', tgt='create_comps' },
      trans{ src='create_comps', tgt='load_behavior', events={ 'e_create_comps_OK'} },
      trans{ src='load_behavior', tgt='configure', events={ 'e_load_behavior_OK'} },
      trans{ src='configure', tgt='connect', events={ 'e_configure_OK'} },
      trans{ src='connect', tgt='run', events={ 'e_connect_OK'} },
   },

   deployment_err = gen_emit_state("stop"),
   running = state{
      A = state {},
      B = state {},
      trans { src='initial', tgt='A' },
      trans { src='A', tgt='B', events = {"e_B"} },
      trans { src='B', tgt='A', events = {"e_A"} },
   },

   cleanup = gen_emit_state("cleanup"),

   trans{ src='initial', tgt='deploy' },
   trans{ src='deploy', tgt='deployment_err', events={ 'e_conf_err'} },
   trans{ src='.deploy.run', tgt='running', events={ 'e_run_OK'} },
   trans{ src='running', tgt='cleanup', events={ 'e_cleanup' } },
}
