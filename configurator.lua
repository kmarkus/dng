-----------------------------
--- Configurator component --
-----------------------------

require "rttlib"
require "libdng"
require "utils"

rttlib.color=1

-- shortcuts
config = libdng.config
import = libdng.import
create = libdng.create
connect = libdng.connect
prop_set = libdng.prop_set
call = libdng.call
port_write = libdng.port_write

tc=rtt.getTC()
tc_name=tc:getName()

-- Component interface spec
iface_spec={
   ports={ 
      { name='conf_events', datatype='string', type='in+event', desc="Configuration events in-port" },
      { name='conf_status', datatype='string', type='out', desc="Current configuration status" },
   },
   properties={
      { name='conf_file', datatype='string', desc="File containing configuration" },
   }
}

iface=rttlib.create_if(iface_spec)

configs=nil
peers=nil
cur_conf=nil
tmpstr=rtt.Variable("string")

local function log(...) print(table.concat({...}, '\t')) end

--- Create configuration component ports and properties.
function configureHook()
   iface=rttlib.create_if(iface_spec)
   local suc, ret
   local conf_file=tc:getProperty("conf_file"):get()
   if not conf_file or conf_file=="" then
      rtt.logl("Error", tc_name..": 'conf_file' property empty")
      return false
   end
   suc, ret = pcall(dofile, conf_file)
   if not suc then
      rtt.logl("Error", tc_name..": failed to read configurations property "..ret)
      return false
   end
   if not ret then
      rtt.logl("Error", tc_name..": failed to read config: nothing returned.")
      return false
   end
   configs=ret
   return true
end

function startHook()
   local depl = rttlib.findpeer("deployer", tc) or rttlib.findpeer("Deployer", tc)
   if not depl then
      rtt.logl("Error", tc_name..": no deployer peer, can't construct peer list.")
      return false
   end
   peers=rttlib.mappeers(function (tc) return tc end, depl)
   return true
end

--- 
function event_to_conf(ev, conf_root)
   assert(conf_root ~= nil, "conf_root must not be nil!")
   local indices = utils.split(ev, "%.") -- index separator is '.'
   local cur = conf_root
   for _,k in ipairs(indices) do
      if cur[k] == nil then return false end
      cur = cur[k]
   end
   return cur
end

function updateHook()
   local fs = iface.ports.conf_events:read(tmpstr)
   if fs=='NewData' then
      local conf_event=tmpstr:tolua()
      local conf=event_to_conf(conf_event, configs)
      if not conf then
	 rtt.logl("Error", tc_name..": unkown configuration '"..conf_event.."' requested.")
	 return
      end
      -- don't re-apply an existing configuration
      if cur_conf==conf_event then
	 log(tc_name..": configuration "..conf_event.." already applied. reapplying.")
      end
      local suc = libdng.apply_conf(conf)
      if suc then
	 log(tc_name..": applied configuration "..conf_event)
	 iface.ports.conf_status:write(conf_event)
	 cur_conf=conf_event
	 iface.ports.conf_status:write("e_"..conf_event.."_OK")
      else
	 rtt.logl("Error", tc_name..": applying configuration "..conf_event.." failed:", errmsg)
	 iface.ports.conf_status:write("e_conf_err")
      end
   end
end

function cleanupHook()
   rttlib.tc_cleanup()
end
