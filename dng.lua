#!/usr/bin/env rttlua-gnulinux
-- -*- lua -*-

require "rttlib"
require "libdng"
require "rfsm"
require "rfsm_rtt"

-- shortcuts
config = libdng.config
import = libdng.import
connect = libdng.connect
create = libdng.create
prop_set = libdng.prop_set
call = libdng.call
port_write = libdng.port_write

rttlib.color=true
local opttab=utils.proc_args(arg)

fsm_file, conf_file, conf_model, peers, Coordinator, Configurator, sim
   = false,false,false,false,false,false,false

tc=rtt.getTC()
d=tc:getPeer("Deployer")

function usage()
   print( [=[
DNG next generation deployer.
usage dng OPTIONS -c <conf file>
   -c           configuration file to launch
   -fsm         rfsm coordination fsm file
   -prefsm      script to prepare the environment before fsm is loaded.
   -check       dont run, just validate configuration file
   -timer       also create a timer component and hook it up with coordinator.
   -sim         do not really launch, just simulate and print results.
   -h           show this.
]=])
end

if #arg==1 or opttab['-h'] then
   usage(); os.exit(1)
end

if not (opttab['-c'] and opttab['-c'][1]) then
   print("no configuration file given (-c option)")
   os.exit(1)
else
   conf_file = opttab['-c'][1]
end

if opttab['-sim'] then
   print("the sim option is yet to be implemented")
   sim = true
end

if opttab['-check'] then
   libdng.check(conf_file, true)
   os.exit(2)
end

--- launch Configurator
Configurator = libdng.launch(conf_file, sim)
req_conf_port=rttlib.port_clone_conn(Configurator:getPort("conf_events"))

--- launch Coordinator
if opttab['-fsm'] then
   fsm_file = opttab['-fsm'][1]
   if not fsm_file then error("-fsm requires an argument"); os.exit(1) end

   prefsm=nil
   if opttab['-prefsm'] then
      prefsm = opttab['-prefsm'][1]
   end

   Coordinator = rfsm_rtt.component_launch_rfsm{
      fsmfile=fsm_file, name='coordinator', deployer=d,
      prestr="rtt.getTC():addEventPort(rtt.InputPort('int'), 'timer_events')",
      prefile=prefsm,
      ev_inport=true, ev_outport=true }

   coord_event= rttlib.port_clone_conn(Coordinator:getPort("events_in"))
   d:connect("coordinator.events_out", "configurator.conf_events", rtt.Variable("ConnPolicy"))
   d:connect("configurator.conf_status", "coordinator.events_in", rtt.Variable("ConnPolicy"))

   function SE_fsm(e)
      coord_event:write(e)
   end

   if opttab['-timer'] then
      d:loadComponent("timer", "OCL::TimerComponent")
      timer = d:getPeer("timer")
      d:connect("timer.timer_0", "coordinator.timer_events", rtt.Variable("ConnPolicy"))
      timer:startTimer(0,0.1)
   end
   Coordinator:configure()
   Coordinator:start()
end

function SE_conf(conf_id)
   req_conf_port:write(conf_id)
   peers = libdng.get_peers() -- update peers
end

function help()
   print([[
   SE_conf(evid)      send event to configurator
   SE_fsm(evid)       send event to FSM (if used)
   upd()              update peers table
]])
end

function upd()
   peers = libdng.get_peers()
end

print("run help() for some information")
