RTT Deployment Next Generation
==============================

Basic idea
----------

Split the coordinatior into

- a slim coordinator that only raises and receives events

- a Configurator, who is configured with a set of configurations. When
  a configuration event is received, the respective configuration is
  applied.

Deployment is then just an other case of Coordination.

Dependencies
------------

- current version (>2.5!) of orocos-ocl
- uMF:  https://github.com/kmarkus/uMF
- rFSM: https://github.com/kmarkus/rFSM


dng tool
--------

The ```dng``` command line tool is the typical way to use the dng
module, which supports deploying configurations in Configurator
components and optionally associated Coordination FSM.

```sh
$ ./dng -h
DNG next generation deployer.
usage dng OPTIONS -c <conf file>
   -c           configuration file to launch
   -fsm         rfsm coordination fsm file
   -prefsm      script to prepare the environment before fsm is loaded.
   -check       dont run, just validate configuration file
   -timer       also create a timer component and hook it up with coordinator.
   -h           show this.
```

After starting a system run `help()` to get help on runtime
commands:

```Lua
> help()
   SE_conf(evid)      send event to configurator
   SE_fsm(evid)       send event to FSM (if used)
   upd()              update peers table
```

A global table `peers` containing all currently known peers is
available.


Configurator-Configuration
--------------------------

The basic structure looks like this:

```Lua
return config {
    conf_name1 = config {
	   pre_conf_state=<switch_statement>,
	   post_conf_state=<switch_statement>,
	   action1{param},
	   action2{param1, param2, ...},
	   ...
    },

    conf_name2 = config {
        ...
    },
   ...
}
```

__Important:__

+ comma after each statement
+ curley brackets!


Post and preconf state specification
------------------------------------

The `pre_conf_state` and `post_conf_state` statements allow to
switch components to certain states before respectively after applying
the configuration. The syntax explained by the following example:

```Lua
pre_conf_state = { "comp1:Running", "comp2:Ignore", "_default:Stopped" }
```

This will switch `comp1` to state Running, leave the state of
`comp2` unchanged and switch all others (`_default`) to state
Stopped prior to applying the configuration. The given order is
respected!

The following states are available:

+ ```PreOperational``` (short ```U```)
+ ```Stopped``` (short ```S```)
+ ```Running``` (short ```R```)
+ ```Ignore``` (short ```I```)


Using the short notation, the above could be written more tersely as:

```Lua
pre_conf_state = { "comp1:R", "comp2:I", "_default:S" }
```


Available actions
-----------------

For RTT, the following actions are available:

### importing packages

```Lua
import{ "pack" }
```

imports package "pack".


### Creating components

```Lua
create{
    name="c1", type="OCL::HelloWorld",
    period=0.01, priority=99, peers={ "c2", "c3" },
	schedt=rtt.globals.ORO_SCHED_RT
}
```

Creates a component. Only `name` and `type` are mandatory.

### Connecting ports

```Lua
connect{
    from="c1.port1", to="c2.serviceA.port2",
    connpol={ type=rtt.globals.BUFFER, size=16 }
}
```

creates a dataflow connections between two ports. Supports services.

`connpol` is a Lua table representing a RTT::ConnPolicy:

```Lua
> print(rtt.Variable("ConnPolicy"))
```

### Setting properties

```Lua
prop_set{"componentA.propZ", value=<value> }
```

`<value>` can a simple Lua type, a Lua table that will be assigned
using `rttlib.varfromtab` or a file-spec using the following
syntax:

```sh
value="file://path/to/conf_file"
```

The file can be either in the "traditional" RTT `cpf` format or
using a json representation. The file extension matters! (Writing json
files can be achieved with the libdng functions
`write_props_json(comp, filename)` or `dump_props({comp1, comp2,...}, dir)`).

To avoid hardcoding absolute paths, the hash '#' syntax can be used to
define package relative paths:

```sh
value="file://package_name#/cpf/file.cpf"
```


### Writing to ports

```Lua
port_write{ port="compA.portX", value }
```

`value` can be a simple Lua type (number, string), or a table that
will be assigned to the RTT type using `rttlib.varfromtab` or an
`rtt.Variable`.

NOTE: the port and connection will be created when used for the first
time. Thus, setting an initial / dummy value during startup will avoid
delays later on.


### Calling operations

Calling operations can be done as follows:

```Lua
call{"compA.Op1", param1, ...}
```

```Lua
call{"compA.ServiceA.op2, param1, param2 }
```


Example
-------

A simple self-contained example can be found in `tests/`:

```sh
./dng -timer -c tests/simple.lua -fsm tests/simple_fsm.lua
```

Todo
----

- Needs some testing.
- Add support for parametric configurations.


Publication
-----------

This software is described in more detail in:

M. Klotzbuecher, G. Biggs, and H. Bruyninckx, "Pure coordination using
the coordinator–configurator pattern" in Proceedings of the 3rd
International Workshop on Domain-Specific Languages and models for
ROBotic systems, November 2012. Japan.


Acknowledgement
---------------

The research leading to these results has received funding from the
European Community's Seventh Framework Programme (FP7/2007-2013) under
grant agreement no. FP7-ICT-231940-BRICS (Best Practice in Robotics)
